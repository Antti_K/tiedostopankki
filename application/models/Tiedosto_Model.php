<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto_Model extends CI_Model {
	public function __construct(){
            parent::__construct();   
            $this->load->helper('directory');
	}
        
        public function hae_kaikki($etsi = "", $limit=NULL, $offset=NULL, $jarjestys=""){  
            if(strlen($etsi)>0){            
                $this->db->or_like('nimi',$etsi);
            }
            $this->db->limit($limit,$offset);
            
            $query = $this->db->get('tiedosto');
            return $query->result();
        }
        public function laske_tiedostot(){
            return $this->db->count_all_results('tiedosto');
        }
        
        public function lisaa($data) {                      
            $this->db->insert('tiedosto',$data);           
            return $this->db->insert_id();
        }
//        public function tallenna_tiedosto($polku){
//            $config['upload_path']=$polku;
//            $config['max_size']='5000';
//            
//            $this->load->library('upload',$config);
//            if(!$this->upload->do_upload()){
//                throw new Exception($this->upload->display_errors());
//            }
//            $tiedoston_tiedot=$this->upload->data();
//            return $tiedoston_tiedot['file_name'];
//        }
        
        
        public function poista($id) {
            //DELETE FROM asiakas WHERE id = $id
            $this->db->where('id',$id);
            $this->db->delete('tiedosto');
        }
        
}
