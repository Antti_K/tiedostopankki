<h3>Tiedostopankki</h3>
<button class="btn"><?php print anchor($uri='tiedosto/lisaa', 'Lisää')?></button>
        <!-- anchor metodilla luodaan <a>-elementti johon tulee linkki lisäyslomakkeelle, linkki näkyy "Lisää" tekstinä-->
        <br>

        <table class="table table-striped">
            <tr>
                <!--<th><?php //echo anchor("tiedosto/index/$sivutuksen_kohta/id", "Id"); ?></th>
                <th><?php //echo anchor("tiedosto/index/$sivutuksen_kohta/nimi", "Nimi"); ?></th>
                <th><?php //echo anchor("tiedosto/index/$sivutuksen_kohta/tiedostonimi", "Tiedostonimi"); ?></th>
                <th><?php //echo anchor("tiedosto/index/$sivutuksen_kohta/kuvaus", "Kuvaus"); ?></th>
                <th><?php //echo anchor("tiedosto/index/$sivutuksen_kohta/tallennettu", "Tallennettu"); ?></th>-->
                <th><?php echo anchor("tiedosto/index/id", "Id"); ?></th>
                <th><?php echo anchor("tiedosto/index/nimi", "Nimi"); ?></th>
                <th><?php echo anchor("tiedosto/index/tiedostonimi", "Tiedostonimi"); ?></th>
                <th><?php echo anchor("tiedosto/index/kuvaus", "Kuvaus"); ?></th>
                <th><?php echo anchor("tiedosto/index/tallennettu", "Tallennettu"); ?></th>
                <th>Poisto</th>
            </tr>
        <?php
        
        foreach($tiedostot as $tiedosto) {            
            print "<tr><td>$tiedosto->id</td>"
                    . "<td>$tiedosto->nimi</td> "
                    . "<td>$tiedosto->tiedostonimi</td> "
                    . "<td>$tiedosto->kuvaus</td> "
                    . "<td>$tiedosto->tallennettu</td> "
                    . "<td>" . anchor("tiedosto/poista/$tiedosto->id", "Poista") . "</td> ";
        }
            ?>
        </table>    
        <?php
            echo $this->pagination->create_links();
        ?>