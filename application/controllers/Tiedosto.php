<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');    
        $this->load->model('tiedosto_model');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
    }

    public function index() {
//        $tiedostoja_sivulla = 5;
//        $jarjestys="";
//        if ($this->session->userdata("jarjestys")){
//            $jarjestys=$this->session->userdata("jarjestys");
//        }
//        if ($this->uri->segment(4)){
//            $jarjestys=$this->uri->segment(4);
//            $this->session->set_userdata("jarjestys",$jarjestys);
//        }
//        $etsi = ""; //samanniminen, muttei sama muuttuja kuin modelissa.
//        $etsi = $this->input->post("search");  
        $data['tiedostot']=$this->tiedosto_model->hae_kaikki('');
//        $data["sivutuksen_kohta"]=0;
//        $config['base_url']=site_url('tiedosto/index');
//        $config['total_rows']=$this->tiedosto_model->laske_tiedostot();
//        $config['per_page']=$asiakkaita_sivulla;
//        $config['uri_segment']=3;
//        $this->pagination->initialize($config);
                
        $data['main_content'] = 'tiedosto_view';
        $this->load->view('template', $data);
    }
    
    
    public function lisaa() {
        $data = array(
            'id' => '',
            'nimi' => '',
            'tiedostonimi' => '',
            'kuvaus' => '',
            'tallennettu' => ''
        );
            $config['upload_path'] = './uploads/';
//            $config['allowed_types'] = 'gif|jpg|png';
//            $config['max_size']	= '100';
//            $config['max_width']  = '1024';
//            $config['max_height']  = '768';

            $this->load->library('upload', $config);

//            if ( ! $this->upload->do_upload()){
//                    $error = array('error' => $this->upload->display_errors());
//                    $this->load->view('upload_form', $error);
//            }else{
//                    //$data = array('upload_data' => $this->upload->data());
//                $data['main_content'] = 'tiedostoLadattu_view';
//                $this->load->view('template', $data);
//            }
        $data['main_content'] = 'tiedostoLisaa_view';
        $this->load->view('template', $data);
    }
    
    function do_upload()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = '*';
//		$config['max_size']	= '100';
//		$config['max_width']  = '1024';
//		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('tiedostonimi'))
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
		}
		else
		{
			//$data = array('upload_data' => $this->upload->data());
                    $data['main_content'] = 'tiedostoLadattu_view';
                    $this->load->view('template', $data);
		}
	}
    
    public function tallenna() {
        $config['upload_path'] = './uploads/';
//		$config['allowed_types'] = 'gif|jpg|png';
//		$config['max_size']	= '100';
//		$config['max_width']  = '1024';
//		$config['max_height']  = '768';

		$this->load->library('upload', $config);

        
        $data = array(
            //tietokannan kenttä => lomakkeen kentän nimi
            'id' => $this->input->post('id'),
            'nimi' => $this->input->post('nimi'),
            'tiedostonimi' => $this->input->post('tiedostonimi'),
            'kuvaus' => $this->input->post('kuvaus'),
            'tallennettu' => $this->input->post('tallennettu')
        );

            $this->tiedosto_model->lisaa($data);       
            redirect('/tiedosto/index');        
    }
    
    public function poista($id) {
        $this->tiedosto_model->poista(intval($id));
        redirect('/tiedosto/index');
        //$this->index();   tällä tavalla osoite riville jää lukemaan ".../poista/[id]"
    }

}