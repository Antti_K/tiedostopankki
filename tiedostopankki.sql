drop database if exists tiedostopankki;

create database tiedostopankki;

use tiedostopankki;

create table tiedosto(
    id int primary key auto_increment,
    nimi varchar(50) not null,
    tiedostonimi varchar(50) not null,
    kuvaus varchar(255),
    tallennettu TIMESTAMP default current_timestamp
);

insert into tiedosto (nimi,tiedostonimi,kuvaus) values ("testi","testing","Kokeillaan");
insert into tiedosto (nimi,tiedostonimi,kuvaus) values ("testi2","testing2","Kokeillaan uudestaan");
